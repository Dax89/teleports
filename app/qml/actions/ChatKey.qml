pragma Singleton
import QtQuick 2.4
import QuickFlux 1.1

KeyTable {
    property string setCurrentChat
    property string createOrOpenPrivateChat
    property string createOrOpenSavedMessages
    property string createOrOpenSecretChat
    property string closeCurrentChat
    property string loadOlderMessages
    property string loadNewerMessages
    property string sendMessage
    property string sendPhoto
    property string sendVideo
    property string sendAudio
    property string sendContact
    property string sendDocument
    property string requestLocation
    property string sendLocation
    property string cancelLocation
    property string stopWaitLocation
    property string sendChatAction
    property string leaveChat
    property string deleteChatHistory
    property string deleteMessage
    property string replyToMessage
    property string requestEditMessage
    property string sendEditMessageText
    property string sendEditMessageCaption
    property string requestReplyToMessage
    property string sendReplyToMessage
    property string showStickerPack
    property string forwardMessage
    property string importFromContentHub
    property string sendImportData
    property string sendForwardMessage
    property string cancelForwardMessage
    property string viewGroupInfo
    property string setCurrentChatById
    property string jumpToMessage
}
